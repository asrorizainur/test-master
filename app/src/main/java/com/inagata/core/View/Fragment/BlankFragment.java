package com.inagata.core.View.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.TextView;

import com.inagata.core.Injector.Component.DaggerFragmentComponent;
import com.inagata.core.Injector.Component.FragmentComponent;
import com.inagata.core.Injector.Module.FragmentModule;
import com.inagata.core.R;
import com.inagata.core.Service.CoreAplication;
import com.inagata.core.View.Base.BaseFragment;

import javax.inject.Inject;

/**
 * A simple {@link Fragment} subclass.
 */
public class BlankFragment extends BaseFragment implements BlankView {
    @Inject
    BlankPresenterImp mPresenter;
    TextView textPertama;

    public BlankFragment() {
        // Required empty public constructor
    }


    @Override
    protected int getContentView() {
        return R.layout.fragment_blank;
    }

    @Override
    protected void initComponents(Bundle savedInstanceState, View view) {
        inject();
        onAttachView();
        setUi(view);
    }

    private void setUi(View view) {
        textPertama = view.findViewById(R.id.textpertama);
    }


    @Override
    public void inject() {
        FragmentComponent mComponent = DaggerFragmentComponent.builder()
                .applicationComponent(CoreAplication.getComponent())
                .fragmentModule(new FragmentModule(this))
                .build();
        mComponent.inject(this);
    }

    @Override
    public void onAttachView() {
        mPresenter.onAttach(this);
    }

    @Override
    public void onDetachView() {
        mPresenter.onDetach();
    }

    @Override
    public void noInternet() {

    }

    @Override
    public void onErrorServiceCallback() {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        onDetachView();
    }
}
