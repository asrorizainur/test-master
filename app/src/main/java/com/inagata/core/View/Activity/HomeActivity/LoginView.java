package com.inagata.core.View.Activity.HomeActivity;

import com.inagata.core.View.Base.IView;

public interface LoginView extends IView {
    void onLoginEror(String identitas_kososng);

    void onLoginSucses(String sukses);
}
