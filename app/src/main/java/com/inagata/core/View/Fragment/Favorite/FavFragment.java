package com.inagata.core.View.Fragment.Favorite;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.inagata.core.Adapter.HomeMovieAdapter;
import com.inagata.core.Adapter.lokalAdapter.FavMovieAdapter;
import com.inagata.core.Injector.Component.DaggerFragmentComponent;
import com.inagata.core.Injector.Component.FragmentComponent;
import com.inagata.core.Injector.Module.FragmentModule;
import com.inagata.core.Model.Lokal.LokalFavMovie;
import com.inagata.core.R;
import com.inagata.core.Service.CoreAplication;
import com.inagata.core.View.Activity.detailMovie.DetailMovieActivity;
import com.inagata.core.View.Base.BaseFragment;
import com.inagata.core.Widget.ItemClickListener;

import java.util.List;

import javax.inject.Inject;

public class FavFragment extends BaseFragment implements FavView, ItemClickListener {
    @Inject
    FavPresenterImp mPresenter;

    public RecyclerView recyclerFav;
    public FavMovieAdapter favAdapter;
    public ImageView kosong;

    public FavFragment() {
    }

    @Override
    protected int getContentView() {
        return R.layout.fragment_fav;
    }

    @Override
    protected void initComponents(Bundle savedInstanceState, View view) {
        inject();
        onAttachView();
        setUi(view);
        init();
    }

    private void init() {
        mPresenter.tampilkanData(kosong);
    }

    @Override
    public void inject() {
        FragmentComponent mComponent = DaggerFragmentComponent.builder()
                .applicationComponent(CoreAplication.getComponent())
                .fragmentModule(new FragmentModule(this))
                .build();
        mComponent.inject(this);
    }

    @Override
    public void onAttachView() {
        mPresenter.onAttach(this);
    }

    @Override
    public void onDetachView() {
        mPresenter.onDetach();
    }

    @Override
    public void noInternet() {

    }

    @Override
    public void onErrorServiceCallback() {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        onDetachView();
    }

    public void setUi(View view) {
        //inisialisasi peratama kali
        recyclerFav= (RecyclerView)view.findViewById(R.id.recyclerFav);
        kosong= (ImageView)view.findViewById(R.id.bgFav);

    }

    @Override
    public void setBg(int size) {
        if (size==0){
            kosong.setVisibility(View.VISIBLE);
        }else{
            kosong.setVisibility(View.GONE);
        }
    }

    @Override
    public void setDataFav(List<LokalFavMovie> favLists) {
        recyclerFav.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager= new LinearLayoutManager(getContext());
        recyclerFav.setLayoutManager(layoutManager);
        favAdapter= new FavMovieAdapter(favLists, getContext());
        recyclerFav.setAdapter(favAdapter);
        favAdapter.setItemClickListener(this);
    }

    @Override
    public void toDetailView(String id_movie, String backdrop_path, String poster_path, String title, String overview, String release_date, String vote_average) {
        Intent pindah= new Intent(getContext(), DetailMovieActivity.class);

        pindah.putExtra(DetailMovieActivity.id, id_movie);
        pindah.putExtra(DetailMovieActivity.title,title);
        pindah.putExtra(DetailMovieActivity.poster_path,poster_path);
        pindah.putExtra(DetailMovieActivity.backdrop_path, backdrop_path);
        pindah.putExtra(DetailMovieActivity.overview,overview);
        pindah.putExtra(DetailMovieActivity.vote_average,vote_average);
        pindah.putExtra(DetailMovieActivity.release_date, release_date);
        pindah.putExtra(DetailMovieActivity.bottomId, "2");
        pindah.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(pindah);
        getActivity().finish();
    }

    @Override
    public void onClick(String position) {
        mPresenter.toDetailPindah(position);
    }
}
