package com.inagata.core.View.Activity.detailMovie;

import com.inagata.core.Model.Lokal.LokalFavMovie;
import com.inagata.core.Model.Lokal.LokalViewMovie;
import com.inagata.core.View.Base.IPresenter;

import java.util.List;

import javax.inject.Inject;

public class DetailMoviePresenterImp implements IPresenter<DetailMovieView>, DetailMoviePresenter  {
    DetailMovieView mView;
    LokalViewMovie views;
    public LokalFavMovie favData;

    @Inject
    public DetailMoviePresenterImp() {
    }

    @Override
    public void onAttach(DetailMovieView view) {
        mView= view;
    }

    @Override
    public void onDetach() {
        mView= null;
    }

    @Override
    public void cekDaftarView(String id) {
        views= new LokalViewMovie();
        if (!views.isViewed(id)){
            mView.setTextBtn("Simpan Data Ditonton");
        }else{
            mView.setTextBtn("Hapus Daftar Tontonan");
        }
    }

    @Override
    public void simpanHapus(String idmvv, String judul, String sinopsis, String rating, String poster, String cover, String tglRilis) {
        views= new LokalViewMovie();
        if (!views.isViewed(idmvv)){
            //menyimpan data
            views= new LokalViewMovie(idmvv, judul, sinopsis, rating, poster, cover, tglRilis);
            views.save();

            List<LokalViewMovie> lihat= views.getAllViews();

            System.out.println("tersimpan view= "+lihat.size());

            mView.setTextBtn("Hapus Daftar Tontonan");
        }else{
            //menghapus data
            views.delById(idmvv);
            mView.setTextBtn("Simpan Data Ditonton");
        }
    }

    @Override
    public void cekFav(String idmvv) {
        favData= new LokalFavMovie();
        if(favData.isFav(idmvv)){
            mView.favTrue(1);
        }else{
            mView.favTrue(0);
        }
    }

    @Override
    public void tambahHapusFav(String idmvv, String judul, String sinopsis, String rating, String poster, String cover, String tglRilis) {
        favData= new LokalFavMovie();
        if(!favData.isFav(idmvv)){
            favData=new LokalFavMovie(idmvv, judul, sinopsis, rating, poster, cover, tglRilis);
            favData.save();
            System.out.println("Data telah disimpan");
            mView.favTrue(1);
        }else{
            favData.delById(idmvv);
            System.out.println("Data dihapus dari Fav");
            mView.favTrue(0);
        }
    }




}
