package com.inagata.core.View.Base;

public interface IPresenter <T extends IView>  {
    void onAttach(T view);

    void onDetach();
}
