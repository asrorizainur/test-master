package com.inagata.core.View.Fragment.Favorite;

import com.inagata.core.Model.Lokal.LokalFavMovie;
import com.inagata.core.View.Base.IView;

import java.util.List;

public interface FavView extends IView {
    void setBg(int size);

    void setDataFav(List<LokalFavMovie> favLists);

    void toDetailView(String id_movie, String backdrop_path, String poster_path, String title, String overview, String release_date, String vote_average);
}
