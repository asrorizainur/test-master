package com.inagata.core.View.Fragment.Home;

import com.inagata.core.Model.Lokal.LokalHomeMovie;
import com.inagata.core.View.Base.IView;

import java.util.List;

public interface HomeView extends IView {
    void setDataMovie(List<LokalHomeMovie> dataMovie);
    void toDetailView(String id, String backdrop_path, String poster_path, String title, String overview, String release_date, String vote_average);

    void dataKosong(String text);

    void show(List<LokalHomeMovie> data);

    void notif(String text);
}