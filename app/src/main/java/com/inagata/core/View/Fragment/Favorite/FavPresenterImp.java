package com.inagata.core.View.Fragment.Favorite;

import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.inagata.core.Model.Lokal.LokalFavMovie;
import com.inagata.core.Model.Lokal.LokalHomeMovie;
import com.inagata.core.Model.Lokal.LokalViewMovie;
import com.inagata.core.Service.BaseApi;
import com.inagata.core.Service.CoreAplication;
import com.inagata.core.View.Base.IPresenter;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class FavPresenterImp implements IPresenter<FavView>, FavPresenter {

    FavView mView;
    public LokalFavMovie favMovie;

    @Inject
    public FavPresenterImp(){

    }

    @Override
    public void onAttach(FavView view) {
        mView= view;
    }

    @Override
    public void onDetach() {
        mView= null;
    }


    @Override
    public void tampilkanData(ImageView kosong) {
        favMovie= new LokalFavMovie();
        List<LokalFavMovie> favLists= favMovie.getAllFavs();
        //System.out.println("data tersimpan "+favLists.size());
        mView.setBg(favLists.size());
        mView.setDataFav(favLists);
    }

    @Override
    public void toDetailPindah(String position) {
        LokalFavMovie lokalFavMovie= LokalFavMovie.getById(position);
        if(lokalFavMovie!=null){
            mView.toDetailView(lokalFavMovie.getId_movie(), lokalFavMovie.getBackdrop_path(), lokalFavMovie.getPoster_path(), lokalFavMovie.getTitle(), lokalFavMovie.getOverview(), lokalFavMovie.getRelease_date(), lokalFavMovie.getVote_average() );
        }else{
            System.out.println("Data Kosong/null id tampil= "+position);
        }
    }


}
