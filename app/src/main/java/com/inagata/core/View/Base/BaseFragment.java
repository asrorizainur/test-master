package com.inagata.core.View.Base;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public abstract class BaseFragment extends Fragment {
    protected View v;

    protected abstract int getContentView();

    protected abstract void initComponents(Bundle savedInstanceState, View view);


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (isAdded() && getActivity() != null) {
            v = inflater.inflate(getContentView(), container, false);
            setHasOptionsMenu(true);
        }



        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initComponents(savedInstanceState, view);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }


}
