package com.inagata.core.View.Fragment.View;

import com.inagata.core.Model.Lokal.LokalViewMovie;
import com.inagata.core.View.Base.IView;

import java.util.List;

public interface ViewView extends IView {
    void setBg(int size);

    void show(List<LokalViewMovie> data);

    void toDetailView(String id_movie, String backdrop_path, String poster_path, String title, String overview, String release_date, String vote_average);
}
