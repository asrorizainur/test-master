package com.inagata.core.View.Fragment.View;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.inagata.core.Adapter.lokalAdapter.FavMovieAdapter;
import com.inagata.core.Adapter.lokalAdapter.ViewMovieAdapter;
import com.inagata.core.Injector.Component.DaggerFragmentComponent;
import com.inagata.core.Injector.Component.FragmentComponent;
import com.inagata.core.Injector.Module.FragmentModule;
import com.inagata.core.Model.Lokal.LokalViewMovie;
import com.inagata.core.R;
import com.inagata.core.Service.CoreAplication;
import com.inagata.core.View.Activity.detailMovie.DetailMovieActivity;
import com.inagata.core.View.Base.BaseFragment;
import com.inagata.core.Widget.ItemClickListener;

import java.util.List;

import javax.inject.Inject;

public class ViewFragment extends BaseFragment implements ViewView, ItemClickListener {
    @Inject
    ViewPresenterImp mPresenter;

    RecyclerView recyclerView;
    ViewMovieAdapter viewAdapter;
    ImageView kosong;

    public ViewFragment() {
    }

    @Override
    protected int getContentView() {
        return R.layout.fragment_view;
    }

    @Override
    protected void initComponents(Bundle savedInstanceState, View view) {
        inject();
        onAttachView();
        setUi(view);
        init();
    }

    private void init() {
        mPresenter.tampilkanView();
    }

    @Override
    public void inject() {
        FragmentComponent mComponent = DaggerFragmentComponent.builder()
                .applicationComponent(CoreAplication.getComponent())
                .fragmentModule(new FragmentModule(this))
                .build();
        mComponent.inject(this);
    }

    @Override
    public void onAttachView() {
        mPresenter.onAttach(this);
    }

    @Override
    public void onDetachView() {
        mPresenter.onDetach();
    }

    @Override
    public void noInternet() {

    }

    @Override
    public void onErrorServiceCallback() {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        onDetachView();
    }

    public void setUi(View view) {
        //code late
        recyclerView= (RecyclerView)view.findViewById(R.id.recycler_View);
        kosong= (ImageView)view.findViewById(R.id.bgView);

    }

    @Override
    public void setBg(int size) {
        if(size<=0){
            kosong.setVisibility(View.VISIBLE);
        }else{
            kosong.setVisibility(View.GONE);
        }
    }

    @Override
    public void show(List<LokalViewMovie> data) {
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager= new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        viewAdapter= new ViewMovieAdapter(data, getContext());
        recyclerView.setAdapter(viewAdapter);
        viewAdapter.setItemClickListener(this);
    }

    @Override
    public void toDetailView(String id_movie, String backdrop_path, String poster_path, String title, String overview, String release_date, String vote_average) {
        Intent pindah= new Intent(getContext(), DetailMovieActivity.class);

        pindah.putExtra(DetailMovieActivity.id, id_movie);
        pindah.putExtra(DetailMovieActivity.title,title);
        pindah.putExtra(DetailMovieActivity.poster_path,poster_path);
        pindah.putExtra(DetailMovieActivity.backdrop_path, backdrop_path);
        pindah.putExtra(DetailMovieActivity.overview,overview);
        pindah.putExtra(DetailMovieActivity.vote_average,vote_average);
        pindah.putExtra(DetailMovieActivity.release_date, release_date);
        pindah.putExtra(DetailMovieActivity.bottomId, "3");
        pindah.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        pindah.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(pindah);
        getActivity().overridePendingTransition(0, 0);
        getActivity().finish();
        getActivity().overridePendingTransition(0, 0);
    }

    @Override
    public void onClick(String position) {
        mPresenter.toDetailMovie(position);
    }
}
