package com.inagata.core.View.Activity.HomeActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.inagata.core.Injector.Component.ActivityComponent;
import com.inagata.core.Injector.Component.DaggerActivityComponent;
import com.inagata.core.Injector.Module.ActivityModule;
import com.inagata.core.R;
import com.inagata.core.Service.CoreAplication;
import com.inagata.core.View.Activity.detailMovie.DetailMovieActivity;
import com.inagata.core.View.Base.BaseActivity;
import com.inagata.core.Widget.Commons;

import javax.inject.Inject;

public class LoginActivty extends BaseActivity implements LoginView {
    @Inject
    LoginPresenterImp mPresenter;
    EditText Edidentitas, Edusername, Edpassword;
    Button Btlogin;

    @Override
    protected int getContentView() {
        return R.layout.activity_main;
    }

    @Override
    protected void initComponents(Bundle savedInstanceState) {
        inject();
        onAttachView();
        setUi();
        initData();
    }

    private void initData() {
        Btlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.gotoValidast(Edidentitas.getText().toString().trim(), Edusername.getText().toString().trim(), Edpassword.getText().toString().trim());
            }
        });

    }

    private void setUi() {
        Edidentitas = findViewById(R.id.identitas);
        Edusername = findViewById(R.id.username);
        Edpassword = findViewById(R.id.password);
        Btlogin = findViewById(R.id.submit_login);
    }


    @Override
    public void inject() {
        ActivityComponent mComponent = DaggerActivityComponent.builder()
                .applicationComponent(CoreAplication.getComponent())
                .activityModule(new ActivityModule(this))
                .build();
        mComponent.inject(this);
    }

    @Override
    public void onAttachView() {
        mPresenter.onAttach(this);
    }

    @Override
    public void onDetachView() {
        mPresenter.onDetach();
    }

    @Override
    public void noInternet() {

    }

    @Override
    public void onErrorServiceCallback() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        onDetachView();
    }

    @Override
    public void onLoginEror(String keterangan) {
        Commons.getInstance().Toast(this, keterangan).show();

    }

    @Override
    public void onLoginSucses(String sukses) {
        Intent intent = new Intent(this, DetailMovieActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        Commons.getInstance().Toast(this, sukses).show();

    }
}