package com.inagata.core.View.Activity.BottomNavigation;

import com.inagata.core.View.Base.IPresenter;

import javax.inject.Inject;

public class BottomNavigationPresenterImp implements IPresenter<BottomNavigationView>, BottomNavigationPresenter{
    public BottomNavigationView mView;

    @Inject
    public BottomNavigationPresenterImp() {

    }

    @Override
    public void onAttach(BottomNavigationView view) {
        mView= view;
    }

    @Override
    public void onDetach() {
        mView= null;
    }
}
