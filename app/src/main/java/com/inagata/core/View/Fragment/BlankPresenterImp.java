package com.inagata.core.View.Fragment;

import com.inagata.core.View.Base.IPresenter;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class BlankPresenterImp implements IPresenter<BlankView>, BlankPresenter {
    @Inject
    public BlankPresenterImp() {
    }

    BlankView mView;
    CompositeDisposable mDisposable;

    @Override
    public void onAttach(BlankView view) {
        mView = view;
        mDisposable = new CompositeDisposable();
    }

    @Override
    public void onDetach() {
        mView = null;
        mDisposable.clear();
    }
}
