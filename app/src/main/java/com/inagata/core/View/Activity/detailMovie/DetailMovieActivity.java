package com.inagata.core.View.Activity.detailMovie;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.inagata.core.Injector.Component.ActivityComponent;
import com.inagata.core.Injector.Component.DaggerActivityComponent;
import com.inagata.core.Injector.Module.ActivityModule;
import com.inagata.core.Model.Lokal.LokalFavMovie;
import com.inagata.core.Model.Lokal.LokalHomeMovie;
import com.inagata.core.R;
import com.inagata.core.Service.CoreAplication;
import com.inagata.core.View.Activity.BottomNavigation.BottomNavigationActivity;
import com.inagata.core.View.Base.BaseActivity;
import com.inagata.core.View.Fragment.BlankFragment;
import com.inagata.core.View.Fragment.Home.HomeFragment;
import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;

import mehdi.sakout.fancybuttons.FancyButton;

import static android.text.Layout.JUSTIFICATION_MODE_INTER_WORD;

public class DetailMovieActivity extends BaseActivity implements DetailMovieView {
    @Inject
    DetailMoviePresenterImp mPresenter;
    TextView judulMovie, sinopsisMovie, tglRilisMovie, rateMovie;
    ImageView posterMovie, coverMovie, backB, favB;
    FancyButton watch;

    public String idmvv, judul, poster, cover, sinopsis, rating, tglRilis, idNavigation;

    HomeFragment refreshData;
    LokalHomeMovie lokalHomeMovie;


    public static String id= "id";
    public static String sortMethod= "sortMethod";
    public static String title= "title";
    public static String poster_path= "poster_path";
    public static String backdrop_path= "backdrop_path";
    public static String overview= "overview";
    public static String vote_average= "vote_average";
    public static String release_date= "release_date";
    public static String bottomId= "bottomid";

    public DetailMovieActivity() {
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_second;
    }

    @Override
    protected void initComponents(Bundle savedInstanceState) {
        inject();
        onAttachView();
        setUi();
        initData();
    }

    private void initData() {

        mPresenter.cekFav(idmvv);

        mPresenter.cekDaftarView(idmvv);

        favB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.tambahHapusFav(idmvv, judul, sinopsis, rating, poster, cover, tglRilis);
            }
        });

        backB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(DetailMovieActivity.this, BottomNavigationActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.putExtra(DetailMovieActivity.bottomId, idNavigation);
                startActivity(intent);
                overridePendingTransition(0, 0);
                finish();
                overridePendingTransition(0, 0);
            }
        });

        watch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.simpanHapus(idmvv, judul, sinopsis, rating, poster, cover, tglRilis);
            }
        });
    }


    private void setUi() {
        lokalHomeMovie= new LokalHomeMovie();
        judulMovie= (TextView)findViewById(R.id.judulMovie);
        posterMovie= (ImageView)findViewById(R.id.poster);
        coverMovie= (ImageView)findViewById(R.id.cover);
        sinopsisMovie= (TextView)findViewById(R.id.sinopsis);
        tglRilisMovie= (TextView)findViewById(R.id.rilis);
        rateMovie= (TextView)findViewById(R.id.rateMovie);
        watch= (FancyButton) findViewById(R.id.addWatchDel);
        backB= (ImageView)findViewById(R.id.backBtn);
        favB= (ImageView)findViewById(R.id.fav);

        idmvv= getIntent().getStringExtra(id).toString();
        judul= getIntent().getStringExtra(title).toString();
        poster= getIntent().getStringExtra(poster_path).toString();
        cover= getIntent().getStringExtra(backdrop_path).toString();
        sinopsis= getIntent().getStringExtra(overview).toString();
        rating= getIntent().getStringExtra(vote_average).toString();
        tglRilis= getIntent().getStringExtra(release_date).toString();
        idNavigation= getIntent().getStringExtra(bottomId).toString();

        judulMovie.setText(judul);
        Picasso.with(this).load("https://image.tmdb.org/t/p/w300_and_h450_bestv2"+poster).into(posterMovie);
        Picasso.with(this).load("https://image.tmdb.org/t/p/w300_and_h450_bestv2"+cover).into(coverMovie);
        sinopsisMovie.setText(sinopsis);
        tglRilisMovie.setText("Release: "+tglRilis);
        rateMovie.setText(""+rating);


    }


    @Override
    public void inject() {
        ActivityComponent mComponent = DaggerActivityComponent.builder()
                .applicationComponent(CoreAplication.getComponent())
                .activityModule(new ActivityModule(this))
                .build();
        mComponent.inject(this);
    }

    @Override
    public void onAttachView() {
        mPresenter.onAttach(this);
    }

    @Override
    public void onDetachView() {
        mPresenter.onDetach();
    }

    @Override
    public void noInternet() {

    }

    @Override
    public void onErrorServiceCallback() {

    }

    @Override
    public void favTrue(int i) {
        if(i==1){
            favB.setImageResource(R.drawable.ic_star_black_24dp);
        }else{
            favB.setImageResource(R.drawable.ic_star_border_black_24dp);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        onDetachView();
    }

    @Override
    public void setTextBtn(String text) {
        watch.setText(text);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent= new Intent(DetailMovieActivity.this, BottomNavigationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.putExtra(DetailMovieActivity.bottomId, idNavigation);
        startActivity(intent);
        overridePendingTransition(0,0);
        finish();
        overridePendingTransition(0, 0);
        }
}
