package com.inagata.core.View.Fragment.Home;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.inagata.core.Adapter.HomeMovieAdapter;
import com.inagata.core.Injector.Component.DaggerFragmentComponent;
import com.inagata.core.Injector.Component.FragmentComponent;
import com.inagata.core.Injector.Module.FragmentModule;
import com.inagata.core.Model.Lokal.LokalHomeMovie;
import com.inagata.core.R;
import com.inagata.core.Service.CoreAplication;
import com.inagata.core.Service.StaticStringList;
import com.inagata.core.View.Activity.detailMovie.DetailMovieActivity;
import com.inagata.core.View.Activity.detailMovie.DetailMovieView;
import com.inagata.core.View.Base.BaseFragment;
import com.inagata.core.Widget.ItemClickListener;
import com.mancj.materialsearchbar.MaterialSearchBar;

import java.util.List;

import javax.inject.Inject;

public class HomeFragment extends BaseFragment implements HomeView, ItemClickListener {
    @Inject
    HomePresenterImp mPresenter;

    RecyclerView recyclerView;
    HomeMovieAdapter adapter;
    MaterialSearchBar materialSearchBar;

    public HomeFragment() {
    }

    @Override
    protected int getContentView() {
        return R.layout.fragment_home;
    }

    @Override
    protected void initComponents(Bundle savedInstanceState, View view) {
        inject();
        onAttachView();
        setUi(view);
        init();
    }

    private void init() {
        materialSearchBar.setHint("Masukan Kata Kunci");
        materialSearchBar.setOnSearchActionListener(new MaterialSearchBar.OnSearchActionListener() {
            @Override
            public void onSearchStateChanged(boolean enabled) {
                mPresenter.cekKoneksi();
            }

            @Override
            public void onSearchConfirmed(CharSequence text) {
                mPresenter.cariData(text);
            }

            @Override
            public void onButtonClicked(int buttonCode) {

            }
        });
    }

    @Override
    public void inject() {
        FragmentComponent mComponent = DaggerFragmentComponent.builder()
                .applicationComponent(CoreAplication.getComponent())
                .fragmentModule(new FragmentModule(this))
                .build();
        mComponent.inject(this);
    }

    @Override
    public void onAttachView() {
        mPresenter.onAttach(this);
    }

    @Override
    public void onDetachView() {
        mPresenter.onDetach();
    }

    @Override
    public void noInternet() {

    }

    @Override
    public void onErrorServiceCallback() {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        onDetachView();
    }

    public void setUi(View view) {
        //inisialisasi data awal
        recyclerView= (RecyclerView)view.findViewById(R.id.movie_recycler);
        materialSearchBar= (MaterialSearchBar)view.findViewById(R.id.searchBar);
        mPresenter.cekKoneksi();
    }

    @Override
    public void setDataMovie(List<LokalHomeMovie> dataMovie) {
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        adapter= new HomeMovieAdapter(dataMovie, getContext());
        recyclerView.setAdapter(adapter);
        adapter.setItemClickListener(this);
    }

    @Override
    public void toDetailView(String idmvv,String backdrop_path, String poster_path, String title, String overview, String release_date, String vote_average) {
        Intent pindah= new Intent(getActivity(), DetailMovieActivity.class);

        pindah.putExtra(DetailMovieActivity.id, idmvv);
        pindah.putExtra(DetailMovieActivity.title,title);
        pindah.putExtra(DetailMovieActivity.poster_path,poster_path);
        pindah.putExtra(DetailMovieActivity.backdrop_path, backdrop_path);
        pindah.putExtra(DetailMovieActivity.overview,overview);
        pindah.putExtra(DetailMovieActivity.vote_average,vote_average);
        pindah.putExtra(DetailMovieActivity.release_date, release_date);
        pindah.putExtra(DetailMovieActivity.bottomId, "1");
        pindah.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        pindah.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(pindah);
        getActivity().overridePendingTransition(0, 0);
        getActivity().finish();
        getActivity().overridePendingTransition(0, 0);

    }

    @Override
    public void dataKosong(String text) {
        Toast.makeText(getContext(), ""+text, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void show(List<LokalHomeMovie> data) {
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        adapter= new HomeMovieAdapter(data, getContext());
        recyclerView.setAdapter(adapter);
        adapter.setItemClickListener(this);
    }

    @Override
    public void notif(String text) {
        Toast.makeText(getContext(), ""+text, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onClick(String position) {
        mPresenter.toDetail(position);
    }

}
