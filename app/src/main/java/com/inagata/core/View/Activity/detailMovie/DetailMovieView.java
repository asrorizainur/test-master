package com.inagata.core.View.Activity.detailMovie;

import com.inagata.core.View.Base.IView;

public interface DetailMovieView extends IView {
    void setTextBtn(String text);

    void favTrue(int i);
}
