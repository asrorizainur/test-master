package com.inagata.core.View.Base;

public interface IView {
    void inject();

    void onAttachView();

    void onDetachView();

    void noInternet();

    void onErrorServiceCallback();
}
