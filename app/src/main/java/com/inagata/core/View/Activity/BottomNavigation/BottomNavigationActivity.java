package com.inagata.core.View.Activity.BottomNavigation;

import android.content.ClipData;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.inagata.core.Injector.Component.ActivityComponent;
import com.inagata.core.Injector.Component.DaggerActivityComponent;
import com.inagata.core.Injector.Module.ActivityModule;
import com.inagata.core.R;
import com.inagata.core.Service.CoreAplication;
import com.inagata.core.View.Base.BaseActivity;
import com.inagata.core.View.Fragment.Favorite.FavFragment;
import com.inagata.core.View.Fragment.Home.HomeFragment;
import com.inagata.core.View.Fragment.View.ViewFragment;

import javax.inject.Inject;

public class BottomNavigationActivity extends BaseActivity implements BottomNavigationView {
    @Inject
    BottomNavigationPresenterImp mPresenter;

    int holdTab= 1;
    public long mBackPressed;
    private static final int TIME_INTERVAL = 2000;

    HomeFragment fragment1;
    FavFragment fragment2;
    ViewFragment fragment3;

    public static String bottomId= "bottomid";

    public String idNavigation;

    android.support.design.widget.BottomNavigationView bottomNavigationView;

    @Override
    protected int getContentView() {
        return R.layout.activity_bottom_navigation;
    }

    @Override
    protected void initComponents(Bundle savedInstanceState) {
        inject();
        onAttachView();
        setUi();
        initData();
    }

    private void initData() {
        bottomNavigationView.setOnNavigationItemSelectedListener(new android.support.design.widget.BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.action_home:
                        showFragment(fragment1);
                        holdTab= 1;
                        break;
                    case R.id.action_fav:
                        showFragment(fragment2);
                        holdTab= 2;
                        break;
                    case R.id.action_view:
                        showFragment(fragment3);
                        holdTab= 3;
                        break;
                }
                return false;
            }
        });
    }

    private void setUi() {
        bottomNavigationView= (android.support.design.widget.BottomNavigationView)findViewById(R.id.bottom_navigation);

        fragment1= new HomeFragment();
        fragment2= new FavFragment();
        fragment3= new ViewFragment();

        if (getIntent()!=null&&getIntent().getExtras()!=null) {
            idNavigation = getIntent().getStringExtra(bottomId).toString();

            if(idNavigation.equals("1")) {
                showFragment(fragment1);
                System.out.println("Home has pressed");
            }else if(idNavigation.equals("2")){
                showFragment(fragment2);
                System.out.println("Fav has pressed");
            }else {
                showFragment(fragment3);
                System.out.println("View has pressed");
            }
        }else{
            showFragment(fragment1);
            System.out.println("Home has pressed");
        }

    }


    @Override
    public void inject() {
        ActivityComponent mComponent = DaggerActivityComponent.builder()
                .applicationComponent(CoreAplication.getComponent())
                .activityModule(new ActivityModule(this))
                .build();
        mComponent.inject(this);
    }

    @Override
    public void onAttachView() {
        mPresenter.onAttach(this);
    }

    @Override
    public void onDetachView() {
        mPresenter.onDetach();
    }

    @Override
    public void noInternet() {

    }

    @Override
    public void onErrorServiceCallback() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        onDetachView();
    }

    @Override
    public void showFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction= getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.navigation_placeholder, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis()||holdTab==1) {
            super.onBackPressed();

        } else {
            showFragment(fragment1);
        }
        mBackPressed = System.currentTimeMillis();

    }
}
