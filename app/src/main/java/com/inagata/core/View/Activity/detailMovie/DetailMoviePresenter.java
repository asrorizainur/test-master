package com.inagata.core.View.Activity.detailMovie;

public interface DetailMoviePresenter {

    void cekDaftarView(String id);


    void simpanHapus(String idmvv, String judul, String sinopsis, String rating, String poster, String cover, String tglRilis);


    void cekFav(String idmvv);

    void tambahHapusFav(String idmvv, String judul, String sinopsis, String rating, String poster, String cover, String tglRilis);

}
