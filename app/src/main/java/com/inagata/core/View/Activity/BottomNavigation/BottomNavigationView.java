package com.inagata.core.View.Activity.BottomNavigation;

import android.support.v4.app.Fragment;

import com.inagata.core.View.Base.IView;

public interface BottomNavigationView extends IView {
    void showFragment(Fragment fragment);
}
