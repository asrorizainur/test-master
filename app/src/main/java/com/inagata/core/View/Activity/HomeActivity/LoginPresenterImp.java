package com.inagata.core.View.Activity.HomeActivity;

import com.inagata.core.Service.BaseApi;
import com.inagata.core.Service.CoreAplication;
import com.inagata.core.View.Base.IPresenter;
import com.inagata.core.Widget.Commons;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class LoginPresenterImp implements IPresenter<LoginView>, LoginPresenter {
    @Inject
    public LoginPresenterImp() {
    }

    LoginView mView;
    CompositeDisposable mDisposable;
    private BaseApi baseAPI;
    private CoreAplication application;

    @Override
    public void onAttach(LoginView view) {
        mView = view;
        mDisposable = new CompositeDisposable();
    }

    @Override
    public void onDetach() {
        mView = null;
        mDisposable.clear();
    }

    @Override
    public void gotoValidast(String identitas, String username, String password) {
        if (Commons.getInstance().notNullNotFill(identitas)) {
            mView.onLoginEror("identitas kososng");
        } else if (Commons.getInstance().notNullNotFill(username)) {
            mView.onLoginEror("username kososng");
        } else if (Commons.getInstance().notNullNotFill(password)) {
            mView.onLoginEror("password kososng");
        } else {
            application = CoreAplication.get(CoreAplication.getComponent().getContext());
            baseAPI = application.getBaseAPIJSON();
            application.clearBaseAPIJSON();
            gotoLogin(identitas, username, password);
        }
    }

    private void gotoLogin(String identitas, String username, String password) {
        application = CoreAplication.get(CoreAplication.getComponent().getContext());
        baseAPI = application.getBaseAPIJSON();
//        if (Commons.getInstance().isConnection()) {
//            mDisposable.add(baseAPI.login(identitas, username, password)
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribeOn(Schedulers.io()).subscribeWith(new DisposableObserver<ModelResponLogin>() {
//                        @Override
//                        public void onNext(ModelResponLogin modelResponLogin) {
//                            if (!modelResponLogin.error && modelResponLogin.data != null) {
//                                Prefs.putString(StaticStringList.TOKEN,modelResponLogin.token);
//                                mView.onLoginSucses("Sukses");
//                            }else {
//                                mView.onLoginEror("gagal Login");
//                            }
//                        }
//
//                        @Override
//                        public void onError(Throwable e) {
//
//                        }
//
//                        @Override
//                        public void onComplete() {
//
//                        }
//                    })
//
//            );
//        } else {
//        }

    }
}
