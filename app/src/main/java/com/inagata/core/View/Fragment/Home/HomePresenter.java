package com.inagata.core.View.Fragment.Home;

public interface HomePresenter {
    void cekKoneksi();

    void cariData(CharSequence text);
}
