package com.inagata.core.View.Fragment.Home;

import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.inagata.core.Adapter.HomeMovieAdapter;
import com.inagata.core.Model.Lokal.LokalHomeMovie;
import com.inagata.core.Model.Lokal.LokalViewMovie;
import com.inagata.core.Model.Response.ModelResponMovie;
import com.inagata.core.Service.BaseApi;
import com.inagata.core.Service.CoreAplication;
import com.inagata.core.Service.StaticStringList;
import com.inagata.core.View.Base.IPresenter;
import com.inagata.core.Widget.Commons;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class HomePresenterImp implements IPresenter<HomeView>, HomePresenter {
    HomeView mView;
    CompositeDisposable mDisposable;
    private BaseApi baseAPI;
    private CoreAplication application;
    LokalHomeMovie movieLists;

    @Inject
    public HomePresenterImp() {

    }

    @Override
    public void onAttach(HomeView view) {
        mView = view;
        mDisposable = new CompositeDisposable();
    }

    @Override
    public void onDetach() {
        mView = null;
        mDisposable.clear();
    }

    @Override
    public void cekKoneksi() {
        application = CoreAplication.get(CoreAplication.getComponent().getContext());
        baseAPI = application.getBaseAPIJSON();
        if (Commons.getInstance().isConnection()) {
            movieLists= new LokalHomeMovie();
            List<LokalHomeMovie> dataMovie = movieLists.getAllMovie();
                   mDisposable.add(baseAPI.getTopRatedMovies(StaticStringList.TOKEN).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribeWith(
                        new DisposableObserver<ModelResponMovie>() {
                            @Override
                            public void onNext(ModelResponMovie modelResponMovie) {
                                System.out.println("xx" + modelResponMovie.getResults().size());
                                LokalHomeMovie.delAll();
                                for (int i = 0; i < modelResponMovie.getResults().size(); i++) {

                                    LokalHomeMovie lokalMovie = new LokalHomeMovie(modelResponMovie.getResults().get(i).getId(),
                                            modelResponMovie.getResults().get(i).getTitle(),
                                            modelResponMovie.getResults().get(i).getOverview(),
                                            modelResponMovie.getResults().get(i).getVoteAverage().toString(),
                                            modelResponMovie.getResults().get(i).getPosterPath(),
                                            modelResponMovie.getResults().get(i).getBackdropPath(),
                                            modelResponMovie.getResults().get(i).getReleaseDate());
                                    lokalMovie.save();
                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                                Toast.makeText(application, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onComplete() {
                                List<LokalHomeMovie> dataMovie = LokalHomeMovie.getAllMovie();
                                if (dataMovie != null && dataMovie.size() != 0) {
                                    mView.setDataMovie(dataMovie);
                                }
                            }
                        }
                        )

                );

        } else {
            //Toast.makeText(application, "Tidak Ada Koneksi Internet", Toast.LENGTH_SHORT).show();
            mView.notif("Tidak ada koneksi Internet");
            movieLists= new LokalHomeMovie();
            List<LokalHomeMovie> data= movieLists.getAllMovie();
            mView.show(data);

        }
    }

    @Override
    public void cariData(CharSequence text) {
        application = CoreAplication.get(CoreAplication.getComponent().getContext());
        baseAPI = application.getBaseAPIJSON();

        if (Commons.getInstance().isConnection()){
            mDisposable.add(baseAPI.getMovieSearch(StaticStringList.TOKEN, text.toString()).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribeWith(
                    new DisposableObserver<ModelResponMovie>() {

                        @Override
                        public void onNext(ModelResponMovie modelResponMovie) {
                            System.out.println("xx" + modelResponMovie.getResults().size());
                            LokalHomeMovie.delAll();
                            for (int i = 0; i < modelResponMovie.getResults().size(); i++) {

                                LokalHomeMovie lokalMovie = new LokalHomeMovie(modelResponMovie.getResults().get(i).getId(),
                                        modelResponMovie.getResults().get(i).getTitle(),
                                        modelResponMovie.getResults().get(i).getOverview(),
                                        modelResponMovie.getResults().get(i).getVoteAverage().toString(),
                                        modelResponMovie.getResults().get(i).getPosterPath(),
                                        modelResponMovie.getResults().get(i).getBackdropPath(),
                                        modelResponMovie.getResults().get(i).getReleaseDate());
                                lokalMovie.save();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            Toast.makeText(application, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onComplete() {
                            List<LokalHomeMovie> dataMovie = LokalHomeMovie.getAllMovie();
                            if (dataMovie != null && dataMovie.size() != 0) {
                                mView.setDataMovie(dataMovie);
                            }else{
                                mView.dataKosong("Data Kosong");
                            }
                        }
                    }
            ));

        }else{
            //cek internet
        }
    }

    public void toDetail(String position) {
        LokalHomeMovie lokalHomeMovie= LokalHomeMovie.getById(position);
        if(lokalHomeMovie!=null){
            mView.toDetailView(lokalHomeMovie.getId_movie(), lokalHomeMovie.getBackdrop_path(), lokalHomeMovie.getPoster_path(), lokalHomeMovie.getTitle(), lokalHomeMovie.getOverview(), lokalHomeMovie.getRelease_date(), lokalHomeMovie.getVote_average() );
        }
    }
}