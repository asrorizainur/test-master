package com.inagata.core.View.Fragment.View;

import com.inagata.core.Model.Lokal.LokalFavMovie;
import com.inagata.core.Model.Lokal.LokalViewMovie;
import com.inagata.core.View.Base.IPresenter;

import java.util.List;

import javax.inject.Inject;

public class ViewPresenterImp implements IPresenter<ViewView>, ViewPresenter {
    ViewView mView;
    LokalViewMovie movies;
    @Inject
    public ViewPresenterImp() {

    }

    @Override
    public void onAttach(ViewView view) {
        mView= view;
    }

    @Override
    public void onDetach() {
        mView=null;
    }

    @Override
    public void tampilkanView() {
        movies= new LokalViewMovie();
        List<LokalViewMovie> data= movies.getAllViews();
        mView.setBg(data.size());
        mView.show(data);
    }

    @Override
    public void toDetailMovie(String position) {
        LokalViewMovie lokalViewMovie= LokalViewMovie.getById(position);
        if(lokalViewMovie!=null){
            mView.toDetailView(lokalViewMovie.getId_movie(), lokalViewMovie.getBackdrop_path(), lokalViewMovie.getPoster_path(), lokalViewMovie.getTitle(), lokalViewMovie.getOverview(), lokalViewMovie.getRelease_date(), lokalViewMovie.getVote_average() );
        }else{
            System.out.println("Data Kosong/null id tampil= "+position);
        }
    }
}
