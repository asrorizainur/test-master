package com.inagata.core.Model.Response;

import com.google.gson.annotations.SerializedName;
import com.inagata.core.Model.ModelMovieVote;

import java.util.List;

public class ModelResponMovie {

    @SerializedName("page")
    private int page;

    @SerializedName("results")
    private List<ModelMovieVote> results;

    @SerializedName("total_results")
    private int totalResults;

    @SerializedName("total_pages")
    private int totalPages;

    public ModelResponMovie() {

    }

    public int getPage() {
        return page;
    }

    public List<ModelMovieVote> getResults() {
        return results;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public int getTotalPages() {
        return totalPages;
    }
}
