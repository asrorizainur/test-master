package com.inagata.core.Model.Response;

import com.google.gson.annotations.SerializedName;

public class ModelResponLogin {

    @SerializedName("error")
    public Boolean error;

    @SerializedName("data")
    public Data data;

    @SerializedName("token")
    public String token;

    public class Data {

        @SerializedName("perusahaan")
        public Perusahaan perusahaan;
        @SerializedName("user")

        public User user;

    }

    public User user;

    public class Perusahaan {

        @SerializedName("id_perusahaan")

        public String idPerusahaan;
        @SerializedName("nama_perusahaan")

        public String namaPerusahaan;
        @SerializedName("nama_direktur")

        public String namaDirektur;
        @SerializedName("kontak")

        public String kontak;
        @SerializedName("email")

        public String email;

    }

    public class User {

        @SerializedName("role")

        public String role;
    }
}
