package com.inagata.core.Model.Lokal;

import android.content.Context;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

import java.util.List;

@Table(name = "LokalFavMovie")
public class LokalFavMovie extends Model{
    @Column(name = "id_movie")
    private String id_movie;

    @Column(name = "title")
    private String title;

    @Column(name = "overview")
    private String overview;

    @Column(name = "vote_average")
    private String vote_average;

    @Column(name = "poster_path")
    private String poster_path;

    @Column(name = "backdrop_path")
    private String backdrop_path;

    @Column(name = "release_date")
    private String release_date;

    public LokalFavMovie() {
    }

    public LokalFavMovie(String id_movie, String title, String overview, String vote_average, String poster_path, String backdrop_path, String release_date) {
        this.id_movie = id_movie;
        this.title = title;
        this.overview = overview;
        this.vote_average = vote_average;
        this.poster_path = poster_path;
        this.backdrop_path = backdrop_path;
        this.release_date = release_date;
    }


    public static List<LokalFavMovie> getAllFavs(){
        return new Select()
                .from(LokalFavMovie.class)
                .orderBy("title ASC")
                .execute();
    }

    public static LokalFavMovie getById(String idMovie) {
        return new Select()
                .from(LokalFavMovie.class)
                .where("id_movie=?",idMovie)

                .executeSingle();
    }


    public static boolean isFav(String id){
        //Select().from(table).execute().size(
        int row= new Select().from(LokalFavMovie.class).where("id_movie=?", id).execute().size();
        if (row<=0){
            return false;
        }else{
            return true;
        }
    }

    public static void delAll(){
        new Delete().from(LokalFavMovie.class).execute();
    }

    public static void delById(String id){
        new Delete().from(LokalFavMovie.class).where("id_movie=?", id).execute();
    }

    public String getId_movie() {
        return id_movie;
    }

    public String getTitle() {
        return title;
    }

    public String getOverview() {
        return overview;
    }

    public String getVote_average() {
        return vote_average;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public String getBackdrop_path() {
        return backdrop_path;
    }

    public String getRelease_date() {
        return release_date;
    }
}
