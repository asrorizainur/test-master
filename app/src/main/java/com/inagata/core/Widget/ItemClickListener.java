package com.inagata.core.Widget;

import android.view.View;

public interface ItemClickListener {
    void onClick(String position);
}
