package com.inagata.core.Adapter.lokalAdapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.inagata.core.Model.Lokal.LokalFavMovie;
import com.inagata.core.R;
import com.inagata.core.View.Fragment.Favorite.FavFragment;
import com.inagata.core.Widget.ItemClickListener;
import com.squareup.picasso.Picasso;

import java.util.List;

public class FavMovieAdapter extends RecyclerView.Adapter<FavMovieAdapter.FavViewHolder>{

    private List<LokalFavMovie> movies;
    private Context context;

    ItemClickListener itemClickListener;

    int adaptorPos;

    public FavMovieAdapter(List<LokalFavMovie> movies, Context context) {
        this.movies = movies;
        this.context = context;
    }



    @NonNull
    @Override
    public FavViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater= LayoutInflater.from(parent.getContext());
        View itemView= inflater.inflate(R.layout.list_item_fav, parent, false);

        return new FavViewHolder(itemView);
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onBindViewHolder(@NonNull final FavViewHolder holder, int position) {
        Picasso.with(context).load("https://image.tmdb.org/t/p/w300_and_h450_bestv2"+movies.get(position).getPoster_path()).into(holder.gambarList);
        holder.JudulList.setText(movies.get(position).getTitle());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adaptorPos= holder.getAdapterPosition();
                if (adaptorPos!=RecyclerView.NO_POSITION&&itemClickListener!=null){
                    itemClickListener.onClick(movies.get(adaptorPos).getId_movie());
                }
            }
        });



    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    public class FavViewHolder extends RecyclerView.ViewHolder{

        public ImageView gambarList;
        public TextView JudulList;
        public ItemClickListener itemClickListener;

        public FavViewHolder(View itemView) {
            super(itemView);

            gambarList= (ImageView)itemView.findViewById(R.id.imgList);
            JudulList= (TextView)itemView.findViewById(R.id.judulList);

        }
    }
}
