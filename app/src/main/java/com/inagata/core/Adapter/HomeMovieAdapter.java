package com.inagata.core.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.inagata.core.Model.Lokal.LokalFavMovie;
import com.inagata.core.Model.Lokal.LokalHomeMovie;
import com.inagata.core.R;
import com.inagata.core.Service.StaticStringList;
import com.inagata.core.Widget.ItemClickListener;
import com.squareup.picasso.Picasso;

import java.util.List;

public class HomeMovieAdapter extends RecyclerView.Adapter<HomeMovieAdapter.HomeViewHolder> {

    private List<LokalHomeMovie> movies;
    private Context context;
    ItemClickListener itemClickListener;

    public LokalFavMovie favData;

    int adaptorPos;

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public HomeMovieAdapter(List<LokalHomeMovie> movies, Context context) {
        this.movies = movies;
        this.context = context;
    }

    @NonNull
    @Override
    public HomeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater= LayoutInflater.from(parent.getContext());
        View itemView= inflater.inflate(R.layout.list_item_movie, parent, false);

        return new HomeViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final HomeViewHolder holder, final int position) {
        favData= new LokalFavMovie();
        Picasso.with(context).load("https://image.tmdb.org/t/p/w300_and_h450_bestv2"+movies.get(position).getBackdrop_path()).resize(200, 250).into(holder.backbg);
        holder.judulMovieR.setText(movies.get(position).getTitle());

        if (favData.isFav(movies.get(position).getId_movie())){
            holder.favIcon.setImageResource(R.drawable.ic_star_black_24dp);
        }

        holder.favIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!favData.isFav(movies.get(position).getId_movie())){
                    //save data
                    favData= new LokalFavMovie(movies.get(position).getId_movie(),
                            movies.get(position).getTitle(),
                            movies.get(position).getOverview(),
                            movies.get(position).getVote_average().toString(),
                            movies.get(position).getPoster_path(),
                            movies.get(position).getBackdrop_path(),
                            movies.get(position).getRelease_date().toString()
                            );
                    favData.save();

                    //List<LokalFavMovie> listFav= favData.getAllFavs();
                    //Toast.makeText(context, "tersimpan "+listFav.size(), Toast.LENGTH_SHORT).show();

                    holder.favIcon.setImageResource(R.drawable.ic_star_black_24dp);
                    //Toast.makeText(context, "Added to Favorite", Toast.LENGTH_SHORT).show();
                }else{
                    //delete data
                    favData.delById(movies.get(position).getId_movie().toString());
                    //Toast.makeText(context, "Removed from Favorite", Toast.LENGTH_SHORT).show();
                    holder.favIcon.setImageResource(R.drawable.ic_star_border_black_24dp);
                }
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adaptorPos=holder.getAdapterPosition();
                if (adaptorPos!=RecyclerView.NO_POSITION&&itemClickListener!=null){
                    itemClickListener.onClick(movies.get(adaptorPos).getId_movie());
                }

            }
        });
    }



    @Override
    public int getItemCount() {
        return movies.size();
    }

    public class HomeViewHolder extends RecyclerView.ViewHolder {
        public ImageView backbg, favIcon;
        public TextView judulMovieR;
        public ItemClickListener itemClickListener;

        public  HomeViewHolder(View itemView) {
            super(itemView);

            backbg= (ImageView)itemView.findViewById(R.id.backbg);
            favIcon= (ImageView)itemView.findViewById(R.id.favAdd);
            judulMovieR= (TextView)itemView.findViewById(R.id.Rjudul);

        }



    }
}
