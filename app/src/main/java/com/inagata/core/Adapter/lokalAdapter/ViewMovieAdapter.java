package com.inagata.core.Adapter.lokalAdapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.inagata.core.Model.Lokal.LokalFavMovie;
import com.inagata.core.Model.Lokal.LokalViewMovie;
import com.inagata.core.R;
import com.inagata.core.View.Fragment.View.ViewFragment;
import com.inagata.core.Widget.ItemClickListener;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ViewMovieAdapter extends RecyclerView.Adapter<ViewMovieAdapter.ViewViewHolder> {

    private List<LokalViewMovie> movies;
    private Context context;

    ItemClickListener itemClickListener;

    int adaptorPos;

    public ViewMovieAdapter(List<LokalViewMovie> movies, Context context) {
        this.movies = movies;
        this.context = context;
    }




    @NonNull
    @Override
    public ViewViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater= LayoutInflater.from(parent.getContext());
        View itemView= inflater.inflate(R.layout.list_item_view, parent, false);

        return new ViewViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewViewHolder holder, int position) {
        Picasso.with(context).load("https://image.tmdb.org/t/p/w300_and_h450_bestv2"+movies.get(position).getPoster_path()).into(holder.gambarView);
        holder.judulView.setText(movies.get(position).getTitle());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adaptorPos= holder.getAdapterPosition();
                if (adaptorPos!=RecyclerView.NO_POSITION&&itemClickListener!=null){
                    itemClickListener.onClick(movies.get(adaptorPos).getId_movie());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public class ViewViewHolder extends RecyclerView.ViewHolder{

        public ImageView gambarView;
        public TextView judulView;
        public ItemClickListener itemClickListener;

        public ViewViewHolder(View itemView) {
            super(itemView);
            gambarView= (ImageView)itemView.findViewById(R.id.imgView);
            judulView= (TextView)itemView.findViewById(R.id.judulView);
        }
    }
}
