package com.inagata.core.Injector.Component;

import android.content.Context;

import com.inagata.core.Injector.Module.ActivityModule;
import com.inagata.core.Injector.Scope.ActivityContext;
import com.inagata.core.Injector.Scope.ActivityScope;
import com.inagata.core.View.Activity.BottomNavigation.BottomNavigationActivity;
import com.inagata.core.View.Activity.HomeActivity.LoginActivty;
import com.inagata.core.View.Activity.detailMovie.DetailMovieActivity;

import dagger.Component;

@ActivityScope
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {
    @ActivityContext
    Context getContext();


    void inject(LoginActivty activity);
    void inject(BottomNavigationActivity activity);
    void inject(DetailMovieActivity activity);
}
