package com.inagata.core.Injector.Component;


import android.app.Application;
import android.content.Context;

import com.inagata.core.Injector.Module.ApplicationModule;
import com.inagata.core.Injector.Module.NetworkModule;
import com.inagata.core.Injector.Scope.ApplicationContext;
import com.inagata.core.Service.BaseApi;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class, NetworkModule.class})
public interface ApplicationComponent {

    @ApplicationContext
    Context getContext();

    void inject(Application mApplication);
    BaseApi getApi();
}
