package com.inagata.core.Injector.Module;


import com.inagata.core.Service.BaseApi;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {
    @Provides
    @Singleton
    public BaseApi provideApi(Retrofit retrofit) {
        return retrofit.create(BaseApi.class);
    }

    @Provides
    @Singleton
    public Retrofit provideRetrofit(OkHttpClient client) {


        return new Retrofit.Builder()
                .client(client)
                // .baseUrl(Prefs.getString(StaticStringList.KEY, BASEURL.BASEURL))
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    @Provides
    @Singleton
    public OkHttpClient provideHttpClient() {
        return new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true)
                .build();
    }
}
