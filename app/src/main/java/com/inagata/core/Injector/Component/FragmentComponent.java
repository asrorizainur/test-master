package com.inagata.core.Injector.Component;


import android.content.Context;

import com.inagata.core.Injector.Module.FragmentModule;
import com.inagata.core.Injector.Scope.ActivityContext;
import com.inagata.core.Injector.Scope.FragmentScope;
import com.inagata.core.View.Fragment.BlankFragment;
import com.inagata.core.View.Fragment.Favorite.FavFragment;
import com.inagata.core.View.Fragment.Home.HomeFragment;
import com.inagata.core.View.Fragment.View.ViewFragment;

import dagger.Component;

@FragmentScope
@Component(dependencies = ApplicationComponent.class, modules = FragmentModule.class)
public interface FragmentComponent {

    @ActivityContext
    Context getContext();

    void inject(BlankFragment fragment);
    void inject(HomeFragment fragment);
    void inject(FavFragment fragment);
    void inject(ViewFragment fragment);

}
