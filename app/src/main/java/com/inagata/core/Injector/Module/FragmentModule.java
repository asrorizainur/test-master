package com.inagata.core.Injector.Module;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.inagata.core.Injector.Scope.ActivityContext;

import dagger.Module;
import dagger.Provides;

@Module
public class FragmentModule {
    private Fragment mFragment;

    public FragmentModule(Fragment mFragment) {
        this.mFragment = mFragment;
    }

    @Provides
    public Fragment provideFragment() {
        return mFragment;
    }

    @Provides
    @ActivityContext
    public Context provideFragmentContext() {
        return mFragment.getContext();
    }
}
