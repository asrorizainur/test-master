package com.inagata.core.Service;

import android.app.Application;
import android.content.Context;
import android.content.ContextWrapper;
import android.support.multidex.MultiDex;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Configuration;
import com.inagata.core.Injector.Component.DaggerApplicationComponent;
import com.inagata.core.Injector.Component.ApplicationComponent;
import com.inagata.core.Injector.Module.ApplicationModule;
import com.pixplicity.easyprefs.library.Prefs;

public class CoreAplication extends Application {

    private static volatile ApplicationComponent mComponent;

    public static synchronized ApplicationComponent getComponent() {
        return mComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ActiveAndroid.initialize(this);
        Configuration dbConfiguration = new Configuration.Builder(this).setDatabaseName("movieDB.db").create();
        ActiveAndroid.initialize(dbConfiguration);
        mComponent = DaggerApplicationComponent.builder().applicationModule(new ApplicationModule(this))
                .build();

        mComponent.inject(this);
        // Initialize the Prefs class
        new Prefs.Builder()
                .setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(getPackageName())
                .setUseDefaultSharedPreference(true)
                .build();
    }

    public BaseApi baseAPIJSON;


    public static CoreAplication get(Context context) {
        return (CoreAplication) context.getApplicationContext();
    }

    public BaseApi getBaseAPIJSON() {
        if (baseAPIJSON == null) {
            System.out.println("??? null");
            baseAPIJSON = BaseApi.Factory.create();
        } else {
            System.out.println("??? gak null");
        }
        return baseAPIJSON;
    }


    public void clearBaseAPIJSON() {
        baseAPIJSON = null;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);
    }
}