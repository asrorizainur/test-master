package com.inagata.core.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.inagata.core.Model.Response.ModelResponMovie;
import com.inagata.core.Widget.LoggingInterceptor;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface BaseApi {



    String BaseUrl = "http://api.themoviedb.org/3/";

    @GET("movie/top_rated")
    Observable<ModelResponMovie> getTopRatedMovies(@Query("api_key") String apiKey);

    @GET("movie/{id}")
    Observable<ModelResponMovie> getMovieDetails(@Path("id") int id, @Query("api_key") String apiKey);

    @GET("search/movie")
    Observable<ModelResponMovie> getMovieSearch(
            @Query("api_key") String apiKey,
            @Query("query") String key

    );
    //https://api.themoviedb.org/3/search/movie?api_key=###&query=the+avengers

    @GET("movie/popular")
    Observable<ModelResponMovie> getPopularMovies(@Query("api_key") String apiKey);

//    @FormUrlEncoded
//    @POST("login")
//
//    Observable<ModelResponLogin> login(@Field("identitas") String identitas, @Field("username") String username, @Field("password") String password);

    class Factory {
        public static BaseApi create() {
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
            clientBuilder.connectTimeout(10, TimeUnit.SECONDS)
                    .readTimeout(10, TimeUnit.SECONDS)
                    .writeTimeout(10, TimeUnit.SECONDS)
                    .retryOnConnectionFailure(true).addInterceptor(new LoggingInterceptor())
                    .build();
            OkHttpClient client = clientBuilder.build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BaseUrl)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(client)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
            return retrofit.create(BaseApi.class);
        }
    }
}
